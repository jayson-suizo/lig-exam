<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });



Route::group(['middleware' => 'auth:api'], function(){
	
	
});

Route::group(['middleware' => 'guest'], function(){
    Route::post("register", "registrationController@register");
	Route::post("login","registrationController@login");
});

Route::resource('posts','blogController',['except' => ['create','edit']]);
Route::resource('posts/{slug}/comments','commentController',['except' => ['create','edit']]);
Route::get('logout', 'registrationController@logout');






