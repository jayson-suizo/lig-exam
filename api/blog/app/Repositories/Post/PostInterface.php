<?php

namespace App\Repositories\Post;

interface postInterface {
	
    public function insert($data);

    public function find($slug);

    public function update($data);

    public function delete($id);

    public function getAll($offset, $limit);
}