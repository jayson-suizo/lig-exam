<?php
namespace App\Repositories\Post;

use App\Repositories\Post\postInterface as postInterface;
use App\Post;

class postRepository implements postInterface
{
    public $post;

    function __construct(Post $post) {
	   $this->post = $post;
    }

    public function insert($data)
    {   
        return $this->post->insertPost($data);
    }

    public function find($slug)
    {   
        return $this->post->findPost($slug);
    }

     public function update($data)
    {
        return $this->post->updatePost($data);
    }

    public function delete($slug)
    {
        return $this->post->deletePost($slug);
    }

    public function getAll($offset, $limit)
    {
        return $this->post->getAll($offset, $limit);
    }


}