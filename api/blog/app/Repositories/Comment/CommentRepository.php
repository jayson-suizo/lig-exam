<?php
namespace App\Repositories\Comment;

use App\Repositories\Comment\commentInterface as commentInterface;
use App\Comment;

class commentRepository implements commentInterface
{
    public $comment;

    function __construct(Comment $comment) {
	   $this->comment = $comment;
    }

    public function insert($data)
    {   
        return $this->comment->insertComment($data);
    }

    public function find($id)
    {   
        return $this->comment->findComment($id);
    }

     public function update($data)
    {
        return $this->comment->updateComment($data);
    }

    public function delete($id)
    {
        return $this->comment->deleteComment($id);
    }

    public function getAll($search)
    {
        return $this->comment->getAll($search);
    }


}