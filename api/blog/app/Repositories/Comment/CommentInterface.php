<?php

namespace App\Repositories\Comment;

interface commentInterface {
	
    public function insert($data);

    public function find($slug);

    public function update($data);

    public function delete($id);

    public function getAll($search);
}