<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
     protected $table = 'comment';

     /**
     * The attributes that are mass assignable.
     *''
     * @var array
     */
    protected $fillable = [
        'body','commentable_type','commentable_id','creator_id','parent_id','title' 
    ];

    public function InsertComment($data)
    {
        return static::create($data);
    }

    public function findComment($id)
    {
        return static::find($id);
    }

    public function updateComment($data)
    {  
        return static::find($data['id'])->update($data);
    }

    public function deleteComment($id)
    {
        return static::find($id)->delete();
    }

    public function getAll($offset = 0, $limit = 10, $search = [])
    {   
        $comment =  new static;

        if(isset($search["parent_id"])) {
            return $comment->where("parent_id",$search["parent_id"])->get();
        } else {
            return [];
        }

        

    }

   
}
