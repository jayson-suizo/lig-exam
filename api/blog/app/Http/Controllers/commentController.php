<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Repositories\Post\postInterface as postInterface;
use App\Repositories\Comment\commentInterface as commentInterface;
use App\Http\Requests\commentRequest;


class commentController extends Controller
{   
    public function __construct(postInterface $post,commentInterface $comment){
        $this->post = $post;
        $this->comment = $comment;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {   $search = [];
        $page = isset($_GET["page"]) ? $_GET["page"]: 1;
        $limit = 10;
        $offset = ($page-1) * 10;
        $find_slug = $this->post->find($slug);

        if($find_slug) { 
            $search["parent_id"] = $find_slug["id"];
            $data = $this->comment->getAll($offset, $limit, $search);
            return response()->json(['data'=> $data],200);


        } else {
            return response()->json(['message' => 'The given data was invalid.','errors'=>'data not found']);
        }

       
        $data = $this->post->getAll($offset, $limit);

        return response()->json(['data'=> $data ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($slug, commentRequest $request)
    {
        $data = Input::all();
        $find_slug = $this->post->find($slug);
        
        if($find_slug) {
            $data["parent_id"] = $find_slug["id"];
            $comment = $this->comment->insert($data);
            return response()->json(['data'=> $comment],200);
        } else {
            return response()->json(['message' => 'The given data was invalid.','errors'=>'data not found']);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug,$id)
    {   
        $comment = $this->comment->find($id);
        
        if($comment){
            return response()->json(['data'=>$comment], 200);
        }else{
            return response()->json(['message' => 'The given data was invalid.','errors'=>'data not found']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($slug,Request $request,$id)
    {   $data = Input::all();
        $find_slug = $this->post->find($slug);
        
        if($find_slug) {
               $find_comment = $this->comment->find($id);
               
               if($find_comment) {
                $data["id"] = $id;
                $this->comment->update($data);
                $updated_comment = $this->comment->find($id);
                return response()->json(['data'=> $updated_comment],200);    
               } else {
                    return response()->json(['message' => 'The given data was invalid.','errors'=>'data not found']);
               }

        } else {
            return response()->json(['message' => 'The given data was invalid.','errors'=>'data not found']);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug,$id)
    {
        $find_slug = $this->post->find($slug);
        if($find_slug){
            $find_comment = $this->comment->find($id);
            
            if($find_comment) {
                $this->comment->delete($id);
                return response()->json(['status'=> 'record deleted successfully'],200);
            } else {
                return response()->json(['message' => 'The given data was invalid.','errors'=>'data not found']);
            }
           
        } else {
            return response()->json(['message' => 'The given data was invalid.','errors'=>'data not found']);
        }

    }
}
