<?php

namespace App\Http\Controllers;
use App\Http\Requests\blogRequest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Repositories\Post\postInterface as PostInterface;

class blogController extends Controller
{   
    public function __construct(postInterface $post){
        $this->post = $post;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = isset($_GET["page"]) ? $_GET["page"]: 1;
        $limit = 10;
        $offset = ($page-1) * 10;
       
        $data = $this->post->getAll($offset, $limit);

        return response()->json(['data'=> $data ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(blogRequest $request)
    {     
        $data = Input::all();
        $post = $this->post->insert($data);
        return response()->json(['data'=> $post],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = $this->post->find($slug);
        
        if($post){
            return response()->json(['data'=>$post], 200);
        }else{
            return response()->json(['message' => 'The given data was invalid.','errors'=>'data not found']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(blogRequest $request, $id)
    {   $data = Input::all();
        $slug = $id;
        $find = $this->post->find($slug);
        if($find) {
            $id = $find->id;
            $data['id'] = $id;
            $update = $this->post->update($data);
            if($update) {
                $new_data = $this->post->find($data['slug']);
                return response()->json(['data'=> $new_data],200);
            }

        } else {
            return response()->json(['message' => 'The given data was invalid.','errors'=>'data not found']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   $slug = $id;
        $find = $this->post->find($slug);
        if($find){
            $delete = $this->post->delete($slug);
            if($delete) {
                return response()->json(['status'=> 'record deleted successfully'],200);
            }
        } else {
            return response()->json(['message' => 'The given data was invalid.','errors'=>'data not found']);
        }
        
    }
}
