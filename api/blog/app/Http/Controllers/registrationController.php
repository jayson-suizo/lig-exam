<?php

namespace App\Http\Controllers;
use App\User;

use Illuminate\Http\Request;
use App\Http\Requests\registrationRequest;
use App\Http\Requests\loginRequest;
use Illuminate\Support\Facades\Input;
use App\Repositories\User\userInterface as UserInterface;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class registrationController extends Controller
{
  public function __construct(UserInterface $user){
    $this->user = $user;
  }
  

  public function register(registrationRequest  $request){	
        $data = Input::all();
        $data["password"] = bcrypt($data["password"]);
        $user = $this->user->insert($data);
        return response()->json(['data'=> $user],200);

  }

  public function login(loginRequest  $request){  
        $data = Input::all();
        $data["password"] = bcrypt($data["password"]);

        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['token_type'] = 'Bearer';
            $success['expire_at'] = $user->createToken('MyApp')->token->expires_at;
            return response()->json(['data' => $success], 200);
        }
        else{

            return response()->json(['error'=>'Unauthorised'], 401);
        }
  }


   /**
   * logout api
   *
   * @return \Illuminate\Http\Response
   */
  public function logout(Request $request)
  {
    
   if (Auth::check()) {
       Auth::user()->AauthAcessToken()->delete();
   }
   
   return response()->json(['data' => "user successfully logout."], 200);
  }



    
}
