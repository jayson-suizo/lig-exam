<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
     protected $table = 'post';

     /**
     * The attributes that are mass assignable.
     *''
     * @var array
     */
    protected $fillable = [
        'title','user_id','content','slug', 
    ];


    public function InsertPost($data)
    {
        return static::create($data);
    }

    public function findPost($slug)
    {
        return static::where('slug',$slug)->first();
    }

    public function updatePost($data)
    {
        return static::find($data['id'])->update($data);
    }


    public function deletePost($slug)
    {
        return static::where('slug',$slug)->delete();
    }

    public function getAll($offset = 0, $limit = 10)
    {   
        $post =  new static;
        $post = $post->offset($offset)->limit($limit);
        return $post->get();

    }


   
}
